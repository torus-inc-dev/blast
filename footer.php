<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blast
 */

?>

			<footer id="colophon" class="site-footer">
				<form id="del-item"></form>
				<form id="checkout"></form>
				<div class="container-xl px-3 mt-3">
					<div class="row row-cards mx-0 ms-sm-1 py-4 border-bottom">
						<div class="col-6 col-md-3 p-0 m-0">
						
						<?php
						$query = '
						query Logo($id: ID!, $idType: MediaItemIdType) {
								mediaItem(id: $id, idType: $idType) {
								sourceUrl
							}
						}';

						$variables = [
							'id' => 'logo',
							'idType' => 'SLUG'
						];

						$data = do_graphql_request( $query, '', $variables);

						$mediaItem = ! empty( $data['data']['mediaItem'] ) ? $data['data']['mediaItem'] : [];					
						?>

							<div class="card card-borderless h-100">
								<div class="card-body d-flex flex-column p-0">
									<img class="card-title w-50 mb-4" src="<?php header_image(); ?>"/>
									<div class="d-flex flex-column justify-content-evenly">
										<span class="pe-sm-4">Trgovački obrt "POVRATNIK" vl. Robert Skender</span>
										<span>OIB: 00000000000</span>
										<span>MB: 91474809</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-md-3 ps-3 p-sm-0 m-0">
							<div class="card card-borderless h-100">
								<div class="card-body d-flex flex-column p-0">
								<?php
								$data = unserialize(apply_filters('header_query','Footer Navigation'));
								$menuItems = $data['data']['menu']['menuItems']['nodes'];
								?>
									<a class="text-decoration-none" href="<?php echo $menuItems[0]['url']; ?>">
										<span class="card-title text-uppercase fs-4 mb-0"><?php echo $menuItems[0]['label']; ?></span>
									</a>
									<div class="d-flex flex-column justify-content-evenly h-75">
									<?php							
									array_shift($menuItems);
									foreach($menuItems as $menuItem):
									?>
										<a class="text-decoration-none" href="<?php echo $menuItem['url']; ?>"><?php echo $menuItem['label']; ?></a>
									<?php
									endforeach;
									?>
									</div>
									<div class="d-flex ms-0 pb-0">
										<a class="btn btn-outline-info px-2 py-1 lh-base" href="https://osm.org/go/0KJqs2QLO?m=&way=401997148" target="_blank" rel="noopener noreferrer">
											<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M12 2C8.13 2 5 5.13 5 9C5 14.25 12 22 12 22C12 22 19 14.25 19 9C19 5.13 15.87 2 12 2ZM12 11.5C10.62 11.5 9.5 10.38 9.5 9C9.5 7.62 10.62 6.5 12 6.5C13.38 6.5 14.5 7.62 14.5 9C14.5 10.38 13.38 11.5 12 11.5Z" fill="#315FAD"/>
											</svg>
										</a>
										<a class="btn btn-outline-info px-2 py-1 lh-base mx-2" href="mailto:webshop@povratnik.hr">
											<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M20 4H4C2.9 4 2.01 4.9 2.01 6L2 18C2 19.1 2.9 20 4 20H20C21.1 20 22 19.1 22 18V6C22 4.9 21.1 4 20 4ZM20 8L12 13L4 8V6L12 11L20 6V8Z" fill="#315FAD"/>
											</svg>
										</a>
										<a class="btn btn-outline-info px-2 py-1 lh-base" href="tel:+38532432342">
											<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M6.62 10.79C8.06 13.62 10.38 15.93 13.21 17.38L15.41 15.18C15.68 14.91 16.08 14.82 16.43 14.94C17.55 15.31 18.76 15.51 20 15.51C20.55 15.51 21 15.96 21 16.51V20C21 20.55 20.55 21 20 21C10.61 21 3 13.39 3 4C3 3.45 3.45 3 4 3H7.5C8.05 3 8.5 3.45 8.5 4C8.5 5.25 8.7 6.45 9.07 7.57C9.18 7.92 9.1 8.31 8.82 8.59L6.62 10.79Z" fill="#315FAD"/>
											</svg>
										</a>
									</div>
								</div>
							</div>

						</div>
						<div class="col-6 col-md-3 p-0 mt-4 m-sm-0">
							<div class="card card-borderless h-100">
								<div class="card-body d-flex flex-column m-0 p-0">
									<a class="text-decoration-none" href="">
										<span class="card-title text-uppercase fs-4 mb-2">Trgovina</span>
									</a>
									<div class="d-flex flex-column justify-content-evenly h-100">
										<a class="text-decoration-none" href="#">Pregled košarice</a>
										<a class="text-decoration-none" href="#">Uvjeti korištenja</a>
										<a class="text-decoration-none" href="#">Pravila privatnosti</a>
										<a class="text-decoration-none" href="#">Prijavite se</a>
										<a class="text-decoration-none" href="#">Registracija</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-md-3 ps-3 p-sm-0 mt-4 m-sm-0">
							<div class="card card-borderless h-100">
								<div class="card-body d-flex flex-column m-0 p-0">
								<?php
								$data = unserialize(apply_filters('header_query','Categories'));
								$menuItems = $data['data']['menu']['menuItems']['nodes'];
								?>
									<a class="text-decoration-none" href="">
										<span class="card-title text-uppercase fs-4 mb-2">Proizvodi</span>
									</a>
									<div class="d-flex flex-column justify-content-evenly h-100">
										<?php
										foreach($menuItems as $menuItem):
										?>
											<a class="text-decoration-none" href="<?php echo sanitize_url( $menuItem['url'], [ 'http', 'https' ]); ?>"><?php echo $menuItem['label']; ?></a>
										<?php
										endforeach;
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row py-3">
						<span class="fs-4 text-center">Povratnik © 2023. Sva prava pridržana</span>
					</div>
				</div>
			</footer><!-- #colophon -->
			<div id="offcanvasBottom" class="offcanvas offcanvas-bottom m-2 mb-0 m-sm-auto bg-none border-top-0" aria-labelledby="offcanvasBottomLabel" role="dialog">
				<div class="container-xl col-12 col-sm-6 my-0 ms-auto me-0 bg-white px-3 px-sm-4">
					<div class="offcanvas-header px-2 pb-2">
						<h4 id="offcanvasBottomLabel" class="offcanvas-title fw-medium text-primary text-uppercase">Odabrani proizvodi:</h4>
						<button class="btn btn-outline-info border me-0 p-2" type="button" data-bs-dismiss="offcanvas" aria-label="Close">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12L19 6.41Z" fill="#315FAD"/>
							</svg>
						</button>
					</div>
					<div style="max-height: 300px;" class="offcanvas-body scrollable bg-white py-0 border-bottom-0 px-2">
						<?php
						$data = unserialize(apply_filters('cart_items',''));
						$cartContents = $data['data']['cart']['contents']['nodes'];
						foreach($cartContents as $cartContent):
						?>
						<input type="hidden" value="<?php echo $cartContent['key'] ?>" name="keys[]">
						<input type="hidden" form="checkout" value="<?php echo $cartContent['product']['node']['productId'] ?>" name="product-id[]">
						<div class="d-flex justify-content-between align-items-center border-bottom pt-3 pb-2">
							<div class="col-8">
								<span class="fw-medium"><?php echo $cartContent['product']['node']['name']; ?></span>
								<div class="row">
								<?php 
								foreach($cartContent['product']['simpleVariations'] as $attributes):
								?>
								<span class="text-muted"><?php echo $attributes['value']; ?></span>
								<?php
								endforeach;
								?>
								</div>

							</div>
							<div class="col-4">
								<div class="d-flex justify-content-between">
									<div class="d-flex">
										<div class="d-flex flex-column h-100">
											<button type="button" id="up" class="btn border-bottom-0 border-end-0 px-1 shadow-none h-50">
												<svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M-6.16331e-08 6.00016L1.41 7.41016L6 2.83016L10.59 7.41016L12 6.00016L6 0.00015614L-6.16331e-08 6.00016Z" fill="#315FAD"/>
												</svg>
											</button>
											<button type="button" id="down" class="btn border-top-0 border-end-0 px-1 shadow-none h-50">
												<svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M12 1.99984L10.59 0.589844L6 5.16984L1.41 0.589844L1.68141e-08 1.99984L6 7.99984L12 1.99984Z" fill="#315FAD"/>
												</svg>
											</button>
										</div>
										<input name="quantity[]" form="checkout" type="number" value="<?php echo sanitize_text_field($cartContent['quantity']) ?>" class="p-0 w-4 me-2 form-control fw-medium text-center border-left-0"/>
										<span class="align-self-center"><?php echo $cartContent['product']['node']['globalAttributes']['edges'][0]['node']['name'] == 'pa_unit' ? $cartContent['product']['node']['globalAttributes']['edges'][0]['node']['options'][0] : ''; ?></span>
									</div>
									<button type="submit" name="remove-from-cart[]" class="btn btn-outline-info p-2 lh-base" value="<?php echo sanitize_text_field($cartContent['key'])?>" formmethod="post" formaction="<?php echo filter_var((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", FILTER_SANITIZE_URL, FILTER_VALIDATE_URL);?>" form="del-item" >
										<svg class="primary-color-icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M6 19C6 20.1 6.9 21 8 21H16C17.1 21 18 20.1 18 19V7H6V19ZM8.46 11.88L9.87 10.47L12 12.59L14.12 10.47L15.53 11.88L13.41 14L15.53 16.12L14.12 17.53L12 15.41L9.88 17.53L8.47 16.12L10.59 14L8.46 11.88ZM15.5 4L14.5 3H9.5L8.5 4H5V6H19V4H15.5Z"/>
										</svg>
									</button>
								</div>								
							</div>
						</div>				
						<?php 
						endforeach;
						?>							
					</div>
					<div class="offcanvas-footer px-1">
						<label class="fs-4 fw-medium mb-2">E-mail adresa:</label>
						<input class="form-control" type="email" form="checkout" name="email" placeholder="ivan.horvat@mail.com" required />
						<label class="fs-4 fw-medium mb-2">Dodatno/detalji (neobavezno):</label>
						<textarea name="customer-note" class="fs-4 w-100 px-3 py-1 form-control h-25" placeholder="Vaša poruka..." form="checkout"></textarea>
						<div class="d-flex flex-wrap mt-2">
							<div class="col-12 col-sm-8">
								<label class="form-check mb-0 mt-2">
									<input class="form-check-input" type="checkbox" form="checkout" required/>
									<span class="form-check-label fs-4 fw-medium">Prihvaćam <a href="#" class="fw-medium btn btn-ghost-warning px-0 fs-4 border-0 border-bottom-wide border-warning">pravila privatnosti.<span class="text-danger">*</span></a></span>
								</label>
							</div>
							<div class="col-7 col-sm-4 ms-0 mt-2 m-sm-0">
								<button name="create-order" type="submit" class="btn btn-primary fw-medium px-1 py-2 fs-4 btn text-uppercase form-control h-full" form="checkout" formmethod="post" formaction="<?php echo filter_var((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", FILTER_SANITIZE_URL,FILTER_VALIDATE_URL);?>">
									<span class="fw-medium fs-3">Pošaljite upit</span>
									<svg class="ms-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M12 5L10.7663 6.23375L15.6488 11.125H5V12.875H15.6488L10.7663 17.7663L12 19L19 12L12 5Z" fill="#FFDC10"/>
									</svg>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div><!-- #page -->

<?php wp_footer(); ?>
	</body>
</html>

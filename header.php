<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blast
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<script type=text/javascript>
		document.cookie = 'window_width='+window.innerWidth+';samesite=lax;secure';
	</script>
</head>

<?php 

$data = unserialize(apply_filters('header_query','Header Navigation'));

$menuItems = $data['data']['menu']['menuItems']['nodes'];

?>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<div class="page">

		<header class="navbar navbar-expand-md py-0">
			<div class="container-xl p-md-3" style="padding: 20px 15px 0px 15px">
				<div class="row justify-content-between align-items-center w-100 border-bottom pt-md-3 pb-md-4" style="padding-bottom: 15px;">
					<h1 class="col-6 col-md-2 navbar-brand navbar-brand-autodark d-none-navbar-horizontal p-0">
						<a href="/"><img class="pe-4 py-1 py-md-0 pe-md-0" src="<?php header_image(); ?>" /></a>
					</h1>
					<div class="d-none d-md-inline col-4 px-4">
						<?php get_search_form(); ?>						
					</div>
					<div class="col-6 d-flex justify-content-between justify-content-md-around px-0 pe-md-2 ps-3 ps-md-2">
						<div class="d-flex flex-row" id="phone">
							<a class="btn btn-outline-info p-2 lh-base" href="">
								<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M7.12 10.9757C8.56 13.8057 10.88 16.1157 13.71 17.5657L15.91 15.3657C16.18 15.0957 16.58 15.0057 16.93 15.1257C18.05 15.4957 19.26 15.6957 20.5 15.6957C21.05 15.6957 21.5 16.1457 21.5 16.6957V20.1857C21.5 20.7357 21.05 21.1857 20.5 21.1857C11.11 21.1857 3.5 13.5757 3.5 4.18573C3.5 3.63573 3.95 3.18573 4.5 3.18573H8C8.55 3.18573 9 3.63573 9 4.18573C9 5.43573 9.2 6.63573 9.57 7.75573C9.68 8.10573 9.6 8.49573 9.32 8.77573L7.12 10.9757Z" fill="#315FAD"/>
								</svg>
							</a>
							<div class="d-none d-md-flex flex-column fs-4 ps-3 fw-medium">
								<span>032 34 56 78</span>
								<span>098 765 43 21</span>
							</div>
						</div>
						<div class="d-flex flex-row location">
							<a class="btn btn-outline-info p-2 lh-base" href="">
								<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M12.25 2.18573C8.38 2.18573 5.25 5.31573 5.25 9.18573C5.25 14.4357 12.25 22.1857 12.25 22.1857C12.25 22.1857 19.25 14.4357 19.25 9.18573C19.25 5.31573 16.12 2.18573 12.25 2.18573ZM12.25 11.6857C10.87 11.6857 9.75 10.5657 9.75 9.18573C9.75 7.80573 10.87 6.68573 12.25 6.68573C13.63 6.68573 14.75 7.80573 14.75 9.18573C14.75 10.5657 13.63 11.6857 12.25 11.6857Z" fill="#315FAD"/>
								</svg>
							</a>
							<div class="d-none d-md-flex flex-column fs-4 ps-3 fw-medium">
								<span>Priljevo 203d,</span>
								<span>32000 Vukovar</span>
							</div>
						</div>
						<div class="d-flex flex-row">
							<a class="btn btn-primary p-2 lh-base" data-bs-toggle="offcanvas" href="#offcanvasBottom" role="button" controls="offcanvasBottom">
								<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 18.1857C6.9 18.1857 6.01 19.0857 6.01 20.1857C6.01 21.2857 6.9 22.1857 8 22.1857C9.1 22.1857 10 21.2857 10 20.1857C10 19.0857 9.1 18.1857 8 18.1857ZM2 2.18573V4.18573H4L7.6 11.7757L6.25 14.2257C6.09 14.5057 6 14.8357 6 15.1857C6 16.2857 6.9 17.1857 8 17.1857H20V15.1857H8.42C8.28 15.1857 8.17 15.0757 8.17 14.9357L8.2 14.8157L9.1 13.1857H16.55C17.3 13.1857 17.96 12.7757 18.3 12.1557L21.88 5.66573C21.96 5.52573 22 5.35573 22 5.18573C22 4.63573 21.55 4.18573 21 4.18573H6.21L5.27 2.18573H2ZM18 18.1857C16.9 18.1857 16.01 19.0857 16.01 20.1857C16.01 21.2857 16.9 22.1857 18 22.1857C19.1 22.1857 20 21.2857 20 20.1857C20 19.0857 19.1 18.1857 18 18.1857Z" fill="#FFDC10"/>
								</svg>
							</a>
							<div class="d-none d-md-flex flex-column fs-4 ps-3 fw-medium">
								<a href="" class="text-decoration-none">Prijavite se</a>
								<a href="" class="text-muted text-decoration-none">Registracija</a>
							</div>
						</div>
					</div>
				</div>							
			</div><!-- .site-branding -->
		</header><!-- #masthead -->
		<header class="navbar navbar-expand-md py-0">
			<div class="container-xl px-md-0 pb-md-3 z-3" style="padding: 20px 15px" >
				<ul class="d-none d-md-flex fw-medium navbar-nav w-100 justify-content-around text-uppercase">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle btn btn-primary text-white" href="#navbar-third" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false">
							<svg class="me-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<rect x="2" y="4" width="20" height="2" fill="#FFDC10"/>
								<rect x="2" y="11" width="20" height="2" fill="#FFDC10"/>
								<rect x="2" y="18" width="20" height="2" fill="#FFDC10"/>
							</svg>
							Kategorije								
						</a>
						<div class="dropdown-menu" data-bs-proper="static">
							<a class="dropdown-item" href="./#">Kategorija 1</a>
							<a class="dropdown-item" href="./#">Kategorija 2</a>
							<a class="dropdown-item" href="./#">Kategorija 3</a>
						</div>
					</li>
					<?php							
					foreach($menuItems as $menuItem):
					?>
					<li class="nav-item">
						<a class="nav-link px-0" href="<?php echo sanitize_url( $menuItem['url'], [ 'http', 'https' ] ); ?>"><?php echo $menuItem['label']; ?></a>
					</li>
					<?php
					endforeach;
					?>
				</ul>
				<div class="d-flex flex-row d-md-none fw-medium navbar-nav w-100 justify-content-around text-uppercase">
					<div class="nav-item dropdown col-5">
						<a class="nav-link dropdown-toggle btn btn-primary fs-4 text-white px-2" href="#navbar-third" style="width: 90%" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false">
							<svg class="me-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<rect x="2" y="4" width="20" height="2" fill="#FFDC10"/>
								<rect x="2" y="11" width="20" height="2" fill="#FFDC10"/>
								<rect x="2" y="18" width="20" height="2" fill="#FFDC10"/>
							</svg>
							Proizvodi								
						</a>
						<div class="dropdown-menu" style="width: 90%" data-bs-proper="static">
						<?php							
						foreach($menuItems as $menuItem):
							?>
							<a class="nav-link" href="<?php echo sanitize_url( $menuItem['url'], [ 'http', 'https' ] ); ?>"><?php echo $menuItem['label']; ?></a>
							<?php
						endforeach;
						?>
						</div>
					</div>
					<div class="col-7 align-self-center">
						<div class="d-sm-none input-icon">
							<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<input id="search-input-phone" type="text" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" class="form-control ps-2 ps-md-3" placeholder="Pretražite proizvode" autocomplete="off" minlength="3">
								<span class="input-icon-addon">
									<!-- Download SVG icon from http://tabler-icons.io/i/search -->
									<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M16.5049 14.4407H15.7149L15.4349 14.1707C16.4149 13.0307 17.0049 11.5507 17.0049 9.94073C17.0049 6.35073 14.0949 3.44073 10.5049 3.44073C6.91488 3.44073 4.00488 6.35073 4.00488 9.94073C4.00488 13.5307 6.91488 16.4407 10.5049 16.4407C12.1149 16.4407 13.5949 15.8507 14.7349 14.8707L15.0049 15.1507V15.9407L20.0049 20.9307L21.4949 19.4407L16.5049 14.4407ZM10.5049 14.4407C8.01488 14.4407 6.00488 12.4307 6.00488 9.94073C6.00488 7.45074 8.01488 5.44073 10.5049 5.44073C12.9949 5.44073 15.0049 7.45074 15.0049 9.94073C15.0049 12.4307 12.9949 14.4407 10.5049 14.4407Z" fill="#315FAD"/>
									</svg>
								</span>
							</form>
						</div>
					</div>
					<script>
						const inputField = document.getElementById('search-input-phone');
						inputField.addEventListener('focus', function() {
							inputField.classList.add('focused');
						})
						inputField.addEventListener('blur', function() {
							inputField.classList.remove('focused');
						})
					</script>
				</div>
			</div>
		</header>
		

<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Blast
 */

?>

<div class="d-none d-sm-block input-icon">
	<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input id="search-input" type="text" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" class="form-control ps-2 ps-md-3" placeholder="Pretražite proizvode" autocomplete="off" minlegth="3">
		<span class="input-icon-addon">
			<!-- Download SVG icon from http://tabler-icons.io/i/search -->
			<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M16.5049 14.4407H15.7149L15.4349 14.1707C16.4149 13.0307 17.0049 11.5507 17.0049 9.94073C17.0049 6.35073 14.0949 3.44073 10.5049 3.44073C6.91488 3.44073 4.00488 6.35073 4.00488 9.94073C4.00488 13.5307 6.91488 16.4407 10.5049 16.4407C12.1149 16.4407 13.5949 15.8507 14.7349 14.8707L15.0049 15.1507V15.9407L20.0049 20.9307L21.4949 19.4407L16.5049 14.4407ZM10.5049 14.4407C8.01488 14.4407 6.00488 12.4307 6.00488 9.94073C6.00488 7.45074 8.01488 5.44073 10.5049 5.44073C12.9949 5.44073 15.0049 7.45074 15.0049 9.94073C15.0049 12.4307 12.9949 14.4407 10.5049 14.4407Z" fill="#315FAD"/>
			</svg>
		</span>
	</form>
</div>
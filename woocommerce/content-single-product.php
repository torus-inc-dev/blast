<?php


$actual_link = sanitize_url((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", [ 'http', 'https' ]);

$parts = parse_url($actual_link);
$path_parts= explode('/', $parts['path']);
$slug = $path_parts[2];



$data = unserialize (do_shortcode( '[product_fetch id="' . $slug . '"]'));

$product = 	! empty( $data['data']['product'] ) ? $data['data']['product'] : [];
$productCategory = ! empty( $product['productCategories']['edges'][0]['node'] ) ? $product['productCategories']['edges'][0]['node'] : [];

?>

<div class="container-xl mb-5">
    <div class="row my-4">
		<div class="col-10">
			<span class="text-primary fs-4 fw-medium">Povratnik</span>
				
			<svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M4.76 1.544L5.908 0.676C6.356 1.41333 6.79467 2.062 7.224 2.622C7.66267 3.17267 8.11067 3.66733 8.568 4.106C9.02533 4.54467 9.51533 4.95533 10.038 5.338V5.422C9.51533 5.79533 9.02533 6.20133 8.568 6.64C8.11067 7.07867 7.66267 7.578 7.224 8.138C6.79467 8.68867 6.356 9.33733 5.908 10.084L4.76 9.202C5.14267 8.614 5.53467 8.08667 5.936 7.62C6.33733 7.15333 6.73867 6.752 7.14 6.416C7.54133 6.07067 7.93333 5.79533 8.316 5.59C8.69867 5.37533 9.06267 5.226 9.408 5.142V5.618C9.06267 5.534 8.69867 5.38467 8.316 5.17C7.93333 4.95533 7.54133 4.68 7.14 4.344C6.73867 3.99867 6.33733 3.59733 5.936 3.14C5.53467 2.67333 5.14267 2.14133 4.76 1.544ZM0.714 4.624H5.18C5.81467 4.624 6.38867 4.64733 6.902 4.694C7.42467 4.74067 7.896 4.80133 8.316 4.876L9.254 5.38L8.316 5.884C7.896 5.94933 7.42467 6.00533 6.902 6.052C6.38867 6.09867 5.81467 6.122 5.18 6.122H0.714V4.624Z" fill="#404040"/>
			</svg>
		
			<span class="text-primary fs-4 fw-medium px-2">Proizvodi</span>
				
			<svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M4.76 1.544L5.908 0.676C6.356 1.41333 6.79467 2.062 7.224 2.622C7.66267 3.17267 8.11067 3.66733 8.568 4.106C9.02533 4.54467 9.51533 4.95533 10.038 5.338V5.422C9.51533 5.79533 9.02533 6.20133 8.568 6.64C8.11067 7.07867 7.66267 7.578 7.224 8.138C6.79467 8.68867 6.356 9.33733 5.908 10.084L4.76 9.202C5.14267 8.614 5.53467 8.08667 5.936 7.62C6.33733 7.15333 6.73867 6.752 7.14 6.416C7.54133 6.07067 7.93333 5.79533 8.316 5.59C8.69867 5.37533 9.06267 5.226 9.408 5.142V5.618C9.06267 5.534 8.69867 5.38467 8.316 5.17C7.93333 4.95533 7.54133 4.68 7.14 4.344C6.73867 3.99867 6.33733 3.59733 5.936 3.14C5.53467 2.67333 5.14267 2.14133 4.76 1.544ZM0.714 4.624H5.18C5.81467 4.624 6.38867 4.64733 6.902 4.694C7.42467 4.74067 7.896 4.80133 8.316 4.876L9.254 5.38L8.316 5.884C7.896 5.94933 7.42467 6.00533 6.902 6.052C6.38867 6.09867 5.81467 6.122 5.18 6.122H0.714V4.624Z" fill="#404040"/>
			</svg>

			<a class="text-primary text-decoration-none fs-4 fw-medium ps-1 pe-2" href="<?php echo sanitize_url( $productCategory['link'], [ 'http', 'https' ] ); ?>"><?php echo sanitize_text_field( $productCategory['name'] ); ?></a>
				
			<svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M4.76 1.544L5.908 0.676C6.356 1.41333 6.79467 2.062 7.224 2.622C7.66267 3.17267 8.11067 3.66733 8.568 4.106C9.02533 4.54467 9.51533 4.95533 10.038 5.338V5.422C9.51533 5.79533 9.02533 6.20133 8.568 6.64C8.11067 7.07867 7.66267 7.578 7.224 8.138C6.79467 8.68867 6.356 9.33733 5.908 10.084L4.76 9.202C5.14267 8.614 5.53467 8.08667 5.936 7.62C6.33733 7.15333 6.73867 6.752 7.14 6.416C7.54133 6.07067 7.93333 5.79533 8.316 5.59C8.69867 5.37533 9.06267 5.226 9.408 5.142V5.618C9.06267 5.534 8.69867 5.38467 8.316 5.17C7.93333 4.95533 7.54133 4.68 7.14 4.344C6.73867 3.99867 6.33733 3.59733 5.936 3.14C5.53467 2.67333 5.14267 2.14133 4.76 1.544ZM0.714 4.624H5.18C5.81467 4.624 6.38867 4.64733 6.902 4.694C7.42467 4.74067 7.896 4.80133 8.316 4.876L9.254 5.38L8.316 5.884C7.896 5.94933 7.42467 6.00533 6.902 6.052C6.38867 6.09867 5.81467 6.122 5.18 6.122H0.714V4.624Z" fill="#404040"/>
			</svg>

			<span class="fs-4 fw-medium"><?php echo $product['name'] ?></span>
		</div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 pe-4">
            <div class="img-responsive" style="background-image: url(<?php echo sanitize_url( $product['image']['sourceUrl'], [ 'http', 'https' ] ); ?>); background-size: contain;"></div>
        </div>
        <div class="col-12 col-sm-6">
			<div class="card border-0 h-100 bg-none">
				<div class="card-body p-0 m-0 d-flex justify-content-between flex-column">
					<div class="d-flex flex-column justify-content-between">
						<p class="card-title text-muted mb-0 fs-4 text-uppercase"><?php echo sanitize_text_field( $productCategory['name'] ); ?></p>
						<h2 class="fw-bolder lh-sm my-2"><?php echo sanitize_text_field( $product['name'] ); ?></h2>
						<p class="lh-sm mb-0"><?php echo str_replace( ["<p>","</p>"], "" , sanitize_text_field( $product['description'] ) ); ?></p>
					</div>
					<div class="d-flex flex-column ms-0">
						<div class="d-flex flex-row mb-4">
							<form class="w-100" method="post">
								<input name="product-id" type="hidden" value="<?php echo sanitize_text_field( $product['databaseId'] ); ?>"/>
								<?php
								$count = 0;
								$atts = 0;
								if(!empty ($product['localAttributes']['edges'])):
									foreach($product['localAttributes']['edges'] as $productAttribute):
									?>
								<div class="d-flex flex-row align-items-center justify-content-between">
									<input name="attribute-name[]" type="hidden" value="<?php echo iconv( 'UTF-8','ASCII//TRANSLIT',strtolower( sanitize_text_field( $productAttribute['node']['name'] ) ) ); ?>">
									<span><?php echo sanitize_text_field( $productAttribute['node']['label'] ) ?></span>
										<?php
										if(sizeof($productAttribute['node']['options']) < 4):
										?>
									<div class="btn-group w-75" role="group">
										<?php
											for($i = 0; $i < sizeof($productAttribute['node']['options']); $i++):
											?>
										<input id="<?php echo $productAttribute['node']['options'][$i]; ?>" class="btn-check" value="<?php echo sanitize_text_field( $productAttribute['node']['options'][$i] ); ?>" type="radio" name="attribute-value[<?php echo $atts; ?>]" autocomplete="off" required>
										<label class="btn py-2" for="<?php echo $productAttribute['node']['options'][$i]; ?>" type="button"><?php echo sanitize_text_field( $productAttribute['node']['options'][$i] ); ?></label>
											<?php
												$count++;
											endfor;
											?>
									</div>
										<?php
										else:
										?>
									<div class="mx-auto">
										<select type="text" class="form-select tomselected ts-hidden-accessible" name="attribute-value[]" id="attribute-value[]" value="" tabindex="-1" style="">
										<?php
											for($i = 0; $i < sizeof($productAttribute['node']['options']); $i++):
											?>
												<option value="<?php echo sanitize_text_field( $productAttribute['node']['options'][$i] ); ?>" class="dropdown-item"><?php echo sanitize_text_field( $productAttribute['node']['options'][$i] ); ?></option>
											<?php
											endfor;
											?>
										</select>
                          			</div>
										<?php
										endif;
										?>
								</div>
										<?php
										$atts++;
									endforeach;
								endif;
								?>
								<div class="d-flex flex-row mt-3 col-sm-6">
									<div class="d-flex flex-column">
										<button type="button" id="up" class="btn border-bottom-0 border-end-0 px-1 shadow-none h-50">
											<svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M-6.16331e-08 6.00016L1.41 7.41016L6 2.83016L10.59 7.41016L12 6.00016L6 0.00015614L-6.16331e-08 6.00016Z" fill="#315FAD"/>
											</svg>
										</button>
										<button type="button" id="down" class="btn border-top-0 border-end-0 px-1 shadow-none h-50">
											<svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M12 1.99984L10.59 0.589844L6 5.16984L1.41 0.589844L1.68141e-08 1.99984L6 7.99984L12 1.99984Z" fill="#315FAD"/>
											</svg>
										</button>
									</div>
									<input name="quantity" type="number" value="1" class="p-0 w-4 form-control fw-medium text-center border-left-0"/>
									<span class="align-self-center mx-2"><?php echo sanitize_text_field( $product['allPaUnit']['nodes'][0]['slug'] ); ?></span>
									<button name="addToCart" type="submit" class="btn btn-primary fw-medium fs-4 py-2 px-0 px-sm-3 btn text-uppercase form-control" formaction="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product['link'] ) );?>">
										<svg class="align-self-stretch" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M19 13.8482H13V19.8482H11V13.8482H5V11.8482H11V5.84821H13V11.8482H19V13.8482Z" fill="#FFDC10"/>
										</svg>
										<span class="fw-medium ps-2">Dodajte u košaricu</span>
									</button>
								</div>
							</form>
						</div>
						<div class="d-flex flex-sm-row flex-column justify-content-between border-top border-bottom py-3">
							<span class="col-12 col-sm-6 fs-4 fw-medium text-primary"><?php echo $product["onSale"] ? "Akcija -30% popusta do " . date_format(date_create($product['dateOnSaleTo']), "d.m.Y") : "" ?></span>
							<span class="col-12 col-sm-6 align-self-sm-end fs-4 fw-medium">Besplatna dostava 30 km</span>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
<?php
if(! empty( $product['upsell']['edges'] )):
?>
<div class="container-xl mb-5 p-0">
	<div class="row section-title">
			<div class="col-7 col-sm-6 p-0">
				<h3 class="fw-medium mb-0 lh-base">
					Povezani proizvodi
				</h3>
			</div>
			<div class="col-5 col-sm-6 d-flex p-0">
				<a class="fw-medium btn btn-ghost-warning lh-1 fs-4 text-uppercase ms-auto me-0 p-0 pb-1 border-0 border-bottom-wide border-warning align-self-end">

				<?php
				if($_COOKIE['window_width'] > 390):
					?>Pogledajte sve proizvode<?php
				else:
					?>Pogledajte sve<?php
				endif;
				?>
					
				</a>
			</div>
		</div>
		<div class="row row-cards justify-content-between products-slick mt-4 mb-3">

		<?php
		
		$upsellEdges = $product['upsell']['edges'];
		
		foreach( $upsellEdges as $upsellEdge ) :
			$node = ! empty( $upsellEdge['node'] ) ? $upsellEdge['node'] : '';
			if ( ! empty($node) && is_array( $node ) ):
		?>

			<div class="card pb-2 border-0">
				<a href="<?php echo sanitize_url( $node['link'] ); ?>">
					<div class="img-responsive img-responsive-3x2 card-img-top" style="background-image: url(<?php echo sanitize_url( $node['image']['sourceUrl'] ); ?>)">
					</div>
				</a>
				<div class="card-body p-2 mx-1 mt-0">
					<a href="<?php echo sanitize_url( $node['productCategories']['edges'][0]['node']['link'] ); ?>">
						<p class="card-title text-muted fs-4 mb-2 pb-1 text-uppercase"><?php echo sanitize_text_field( $node['productCategories']['edges'][0]['node']['name'] ); ?></p>
					</a>
					<h4 class="fw-medium lh-sm h-5 mb-0"><?php echo sanitize_text_field( $node['name'] ); ?></h4>
					<div class="d-flex justify-content-between ms-0 pb-2">
						<a class="btn btn-outline-info fw-medium fs-4 btn text-uppercase" href="<?php echo sanitize_url( $node['link'] ); ?>">
							<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM11 15H9V9H11V15ZM11 7H9V5H11V7Z" fill="#315FAD"/>
							</svg>
							<span class="ms-2">Provjerite</span>
						</a>
						<a class="btn btn-primary px-2 py-1 lh-base">
							<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M19 13.8482H13V19.8482H11V13.8482H5V11.8482H11V5.84821H13V11.8482H19V13.8482Z" fill="#FFDC10"/>
							</svg>
						</a>
					</div>
				</div>
			</div>

		<?php
			endif;
		endforeach;
		?>
	</div>
</div>
<?php
endif;
?>
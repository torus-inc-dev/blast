<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blast
 */

get_header();
?>

	<main id="primary" class="site-main">
		<div class="container-xl px-0">
			<div class="row my-4">
				<div class="col-5">
					<span class="text-primary fs-4 fw-medium pe-1">Povratnik</span>
						
					<svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M4.76 1.544L5.908 0.676C6.356 1.41333 6.79467 2.062 7.224 2.622C7.66267 3.17267 8.11067 3.66733 8.568 4.106C9.02533 4.54467 9.51533 4.95533 10.038 5.338V5.422C9.51533 5.79533 9.02533 6.20133 8.568 6.64C8.11067 7.07867 7.66267 7.578 7.224 8.138C6.79467 8.68867 6.356 9.33733 5.908 10.084L4.76 9.202C5.14267 8.614 5.53467 8.08667 5.936 7.62C6.33733 7.15333 6.73867 6.752 7.14 6.416C7.54133 6.07067 7.93333 5.79533 8.316 5.59C8.69867 5.37533 9.06267 5.226 9.408 5.142V5.618C9.06267 5.534 8.69867 5.38467 8.316 5.17C7.93333 4.95533 7.54133 4.68 7.14 4.344C6.73867 3.99867 6.33733 3.59733 5.936 3.14C5.53467 2.67333 5.14267 2.14133 4.76 1.544ZM0.714 4.624H5.18C5.81467 4.624 6.38867 4.64733 6.902 4.694C7.42467 4.74067 7.896 4.80133 8.316 4.876L9.254 5.38L8.316 5.884C7.896 5.94933 7.42467 6.00533 6.902 6.052C6.38867 6.09867 5.81467 6.122 5.18 6.122H0.714V4.624Z" fill="#404040"/>
					</svg>
				
					<span class="text-decoration-none fs-4 fw-medium px-1">Kontakt</span>
				</div>
			</div>
			<div class="row w-100 px-3 mx-1 map mb-5">
				<div class="col-lg-6 ps-5 ms-5 h-100">
					<h2 class="ms-4 fw-bolder lh-sm">Kontakt informacije</h2>
					<div class="d-flex ms-4 mt-4">
						<a class="btn btn-outline-info px-3 py-2 fw-normal fs-3 text-uppercase" href="#">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M20 4H4C2.9 4 2.01 4.9 2.01 6L2 18C2 19.1 2.9 20 4 20H20C21.1 20 22 19.1 22 18V6C22 4.9 21.1 4 20 4ZM20 8L12 13L4 8V6L12 11L20 6V8Z" fill="#315FAD"></path>
							</svg>
							<span class="ms-2 text-uppercase text-secondary-fg">E-mail</span>
						</a>
						<a class="fw-medium btn btn-ghost-warning lh-1 fs-4 ms-3 me-0 p-0 border-0 border-bottom-wide border-warning align-self-end pb-2 mb-1">povratnik@povratnik.hr</a>
					</div>
					<div class="d-flex ms-4 my-3 py-1">
						<a class="btn btn-outline-info fw-normal fs-3 px-3 py-2 text-uppercase" href="#">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M6.62 10.79C8.06 13.62 10.38 15.93 13.21 17.38L15.41 15.18C15.68 14.91 16.08 14.82 16.43 14.94C17.55 15.31 18.76 15.51 20 15.51C20.55 15.51 21 15.96 21 16.51V20C21 20.55 20.55 21 20 21C10.61 21 3 13.39 3 4C3 3.45 3.45 3 4 3H7.5C8.05 3 8.5 3.45 8.5 4C8.5 5.25 8.7 6.45 9.07 7.57C9.18 7.92 9.1 8.31 8.82 8.59L6.62 10.79Z" fill="#315FAD"></path>
							</svg>
							<span class="ms-2 text-uppercase text-secondary-fg">Telefon</span>
						</a>
						<a class="fw-medium btn btn-ghost-warning lh-1 fs-4 ms-3 me-0 p-0 border-0 border-bottom-wide border-warning align-self-end pb-2 mb-1">032 45 67 89</a>
						<a class="fw-medium btn btn-ghost-warning lh-1 fs-4 ms-3 me-0 p-0  border-0 border-bottom-wide border-warning align-self-end pb-2 mb-1">098 76 54 321</a>
					</div>
					<div class="d-flex ms-4">
						<a class="btn btn-outline-info fw-normal fs-3 px-3 py-2 text-uppercase" href="#">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M12 2C8.13 2 5 5.13 5 9C5 14.25 12 22 12 22C12 22 19 14.25 19 9C19 5.13 15.87 2 12 2ZM12 11.5C10.62 11.5 9.5 10.38 9.5 9C9.5 7.62 10.62 6.5 12 6.5C13.38 6.5 14.5 7.62 14.5 9C14.5 10.38 13.38 11.5 12 11.5Z" fill="#315FAD"></path>
							</svg>
							<span class="ms-2 text-uppercase text-secondary-fg">Lokacija</span>
						</a>
						<a class="fw-medium btn btn-ghost-warning lh-1 fs-4 ms-3 me-0 p-0 border-0 border-bottom-wide border-warning align-self-end pb-2 mb-1">Priljevo 203d, 32000 Vukovar, Hrvatska</a>
					</div>
				</div>
				<div class="col-lg-5 px-0 ms-2 h-100">
					<object	class="h-100" width="488" data="https://www.openstreetmap.org/export/embed.html?bbox=18.966712653636936%2C45.365828079446764%2C18.971969783306122%2C45.368219408569026&amp;layer=mapnik&amp;marker=45.36702375664448%2C18.969341218471527"></object>
				</div>
			</div>
			<div class="row w-100 px-3 mx-1 mt-4">
				<div class="col-lg-6 ps-5 ms-5 h-100">
					<h2 class="ms-4 fw-bolder lh-sm">Pošaljite nam upit</h2>
					<p class="ms-4 mt-3 pt-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
				<div class="wp-block-column col-lg-5 px-0 ms-2 h-100 is-layout-flow wp-block-column-is-layout-flow">
					<form method="post" action="<?php echo filter_var((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", FILTER_SANITIZE_URL,FILTER_VALIDATE_URL);?>">
						<div class="p-0">
							<div class="mb-4">
								<label class="fs-4 fw-medium">E-mail adresa <span class="text-danger">*</span></label>
								<input class="form-control mt-2 mb-3" type="email" name="replyTo" placeholder="ivan.horvat@mail.com" required="">
								<label class="fs-4 fw-medium">Predmet upita <span class="text-danger">*</span></label>
								<input class="form-control mt-2 mb-3" type="text" name="subject" placeholder="" required="">
								<textarea name="body" class="fs-4 w-100 form-control h-25" placeholder="Vaša poruka..."></textarea>
							</div>
							<?php echo do_shortcode('[hcaptcha auto="true"]'); ?>
							<div class="d-flex mt-3">
								<div class="d-flex col-8">
									<label class="form-check mb-0 mt-2">
										<input class="form-check-input" type="checkbox" required=""><span class="form-check-label fs-4 fw-medium">Prihvaćam <a href="#" class="fw-medium btn btn-ghost-warning px-0 fs-4 border-0 border-bottom-wide border-warning">pravila privatnosti.<span class="text-danger">*</span>
									</a></label>
								</div>
							<div class="col-4 ms-0">
								<button name="send" type="submit" class="btn btn-primary fw-medium px-1 py-2 fs-4 btn text-uppercase form-control h-full justify-content-evenly">
									<span class="fw-medium">Pošaljite</span>
									<svg class="ms-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M12 5L10.7663 6.23375L15.6488 11.125H5V12.875H15.6488L10.7663 17.7663L12 19L19 12L12 5Z" fill="#FFDC10"></path>
									</svg>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="container-xl p-0 scroll-top">
			<div class="row my-4">
				<div class="d-flex justify-content-end p-0">
					<a class="btn btn-outline-info p-2 lh-base" href="#">
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M4 12L5.41 13.41L11 7.83L11 20L13 20L13 7.83L18.59 13.41L20 12L12 4L4 12Z" fill="#315FAD"/>
						</svg>
					</a>
				</div>
			</div>
		</div>
	</main><!-- #main -->

<?php
get_footer();

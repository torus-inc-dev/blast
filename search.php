<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Blast
 */
$currentPageNumber = ! empty($_GET['page']) ? (int)$_GET['page'] : 1;

$actual_link = sanitize_url((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", [ 'http', 'https' ] );

$trim = strstr($actual_link, '&', true) ? strstr($actual_link, '&', true) : $actual_link;

$parts = parse_url($actual_link);
$path_parts= explode('/', $parts['path']);

$svg = "data:image/svg+xml,%3Csvg width='11' height='11' viewBox='0 0 11 11' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M4.76 1.544L5.908 0.676C6.356 1.41333 6.79467 2.062 7.224 2.622C7.66267 3.17267 8.11067 3.66733 8.568 4.106C9.02533 4.54467 9.51533 4.95533 10.038 5.338V5.422C9.51533 5.79533 9.02533 6.20133 8.568 6.64C8.11067 7.07867 7.66267 7.578 7.224 8.138C6.79467 8.68867 6.356 9.33733 5.908 10.084L4.76 9.202C5.14267 8.614 5.53467 8.08667 5.936 7.62C6.33733 7.15333 6.73867 6.752 7.14 6.416C7.54133 6.07067 7.93333 5.79533 8.316 5.59C8.69867 5.37533 9.06267 5.226 9.408 5.142V5.618C9.06267 5.534 8.69867 5.38467 8.316 5.17C7.93333 4.95533 7.54133 4.68 7.14 4.344C6.73867 3.99867 6.33733 3.59733 5.936 3.14C5.53467 2.67333 5.14267 2.14133 4.76 1.544ZM0.714 4.624H5.18C5.81467 4.624 6.38867 4.64733 6.902 4.694C7.42467 4.74067 7.896 4.80133 8.316 4.876L9.254 5.38L8.316 5.884C7.896 5.94933 7.42467 6.00533 6.902 6.052C6.38867 6.09867 5.81467 6.122 5.18 6.122H0.714V4.624Z' fill='%23404040'/%3E%3C/svg%3E";

get_header();

//wc_get_template( 'archive-product.php' );


?>
<script>
// var uri = '<?php echo $actual_link; ?>';

// if (uri.indexOf("&after") > 0) {
// 	var clean_uri = uri.substring(0, uri.indexOf("&after"));
// 	window.history.replaceState({}, document.title, clean_uri);
// }
</script>
<div class="container-xl p-0">
	<div class="row my-4">
		<div class="">
			<span class="text-primary fs-4 fw-medium pe-1">Povratnik</span>				
			<img src="<?php echo $svg; ?>" alt="" />		
			<span class="text-primary fs-4 fw-medium px-1">Proizvodi</span>				
	    	<img src="<?php echo $svg; ?>" alt="" />
			<span class="text-decoration-none fs-4 fw-medium px-1"><?php echo sanitize_text_field ( $_GET['s'] ); ?></span>
		</div>
	</div>
	<div class="row mb-3">
		<div class="row row-cards mt-0">
		<?php

		if($_COOKIE['window_width'] > 390) {
			$per_page = 12;
		}
		else {
			$per_page = 1;
		}
		
		
		$first = isset( $_GET['before'] ) ? null : $per_page;
		$last = isset( $_GET['before'] ) ? $per_page : null;
		$before = isset( $_GET['before'] ) ? sanitize_text_field( $_GET['before'] ) : null;
		$after = isset( $_GET['after'] ) ? sanitize_text_field( $_GET['after'] ) : null;



		$data = unserialize (do_shortcode( '[product_search_query search="' . sanitize_text_field( $_GET['s'] ) . '" first="' . $first . '" last="' . $last . '" before="' . $before . '" after="' . $after .']' ));
		
		$products = ! empty( $data['data']['products']['edges'] ) ? $data['data']['products']['edges'] : [];
		
		$currentPage = ! empty( $data['data']['products']['pageInfo'] ) ? $data['data']['products']['pageInfo'] : [];
		$hasPreviousPage = $currentPage['hasPreviousPage'];
		$hasNextPage = $currentPage['hasNextPage'];
		$startCursor = $currentPage['startCursor'];
		$endCursor = $currentPage['endCursor'];

		foreach( $products as $product ) :
			$node = ! empty( $product['node'] ) ? $product['node'] : '';
			if ( ! empty($node) && is_array( $node ) ):
			?>
			<div class="col-6 col-sm-3">
				<div class="card pb-2 border-0">
					<a href="<?php echo $node['link'] ?>">
						<div class="img-responsive img-responsive-3x2 card-img-top" style="background-image: url(<?php echo sanitize_url( $node['image']['sourceUrl'], [ 'http', 'https' ] ); ?>); background-size: contain;">
						</div>
					</a>
					<div class="card-body p-2 p-sm-3 mx-1 mt-0">
						<a href="<?php echo sanitize_url( $node['productCategories']['edges'][0]['node']['link'], [ 'http', 'https' ] ); ?>">
							<p class="card-title text-muted fs-4 mb-2 pb-1 text-uppercase"><?php echo $node['productCategories']['edges'][0]['node']['name'] ?></p>
						</a>
						<h4 class="fw-medium lh-sm h-5 mb-0"><?php echo $node['name'] ?></h4>
						<div class="d-flex justify-content-between ms-0 pb-2">
							<a class="btn btn-outline-info fw-medium fs-4 btn text-uppercase" href="<?php echo sanitize_url( $node['link'], [ 'http','https' ] ); ?>">
								<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM11 15H9V9H11V15ZM11 7H9V5H11V7Z" fill="#315FAD"/>
								</svg>
								<span class="ms-2 d-none d-sm-block">Provjerite</span>
								<span class="ms-2 d-sm-none">Info</span>
							</a>
							<a class="btn btn-primary px-2 py-1 lh-base">
								<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M19 13.8482H13V19.8482H11V13.8482H5V11.8482H11V5.84821H13V11.8482H19V13.8482Z" fill="#FFDC10"/>
								</svg>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php
			endif;
		endforeach;

		?>
		</div>
		<?php
		if($hasNextPage == true || $hasPreviousPage == true):
		?>
		<div class="row">
			<div class="d-flex justify-content-center">
				<ul class="pagination mt-4 flex-row-reverse">					
				<?php
				for($pageNumber = $currentPageNumber - 1; $pageNumber > 0; $pageNumber--):			
						?>
						<li class="page-item">
							<a class="page-link fs-3 border-primary fw-medium" href="<?php echo ( (int)$pageNumber != 1 ) ? sanitize_url( htmlspecialchars($trim . '&page=' . $pageNumber . '&before=' . $startCursor, ENT_QUOTES, 'UTF-8'), [ 'http', 'https' ] ) : sanitize_url( htmlspecialchars($trim), [ 'http', 'https' ] ) ?>">
							<?php echo $pageNumber; ?>
							</a>
						</li>
					<?php
					$data = unserialize (do_shortcode( '[previous_page search="' . sanitize_text_field( $_GET['s'] ) . '" last="' . htmlspecialchars($per_page, ENT_QUOTES, 'UTF-8') . '" before="' . htmlspecialchars($startCursor, ENT_QUOTES, 'UTF-8') . '"]' ));
					
					$pagination = ! empty( $data['data']['products']['pageInfo'] ) ? $data['data']['products']['pageInfo'] : [];
					
					$hasPreviousPage = ! empty( $pagination['hasPreviousPage'] ) ? $pagination['hasPreviousPage'] : false;
					$startCursor = ! empty( $pagination['startCursor'] ) ? $pagination['startCursor'] : '';
				endfor;
				if(!$currentPage['hasPreviousPage']):
				?>
					<li class="page-item disabled">
				<?php 
				else:
				?>
					<li class="page-item ">
						<a class="page-link" href="<?php echo $currentPage['hasPreviousPage'] && $currentPageNumber != 2 ? sanitize_url( $trim . '&page=' . $currentPageNumber - 1 . '&before=' . $currentPage['startCursor'], ENT_QUOTES, 'UTF-8', [ 'http', 'https' ] ) : sanitize_url( $trim, ['http', 'https'] ) ?>">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<rect x="0.5" y="0.5" width="23" height="23" fill="#FBFBFB"/>
								<path d="M15.7049 7.41L14.2949 6L8.29492 12L14.2949 18L15.7049 16.59L11.1249 12L15.7049 7.41Z" fill="#262626"/>
								<rect x="0.5" y="0.5" width="23" height="23" stroke="#E0E0E0"/>
							</svg>
						</a>
				<?php
				endif;
				?>
					</li>
				</ul>
				<ul class="pagination mt-4">
					<li class="page-item">
						<a class="page-link fs-3 border-primary fw-medium active">
							<?php echo $currentPageNumber; ?>
						</a>
					</li>
				<?php
				$pageNumber = $currentPageNumber;
				while($hasNextPage):
					$pageNumber++;
					
					?>
					<li class="page-item">
						<a class="page-link fs-3 border-primary fw-medium" href="<?php echo sanitize_url( htmlspecialchars($trim . '&page=' . $pageNumber . '&after=' . $endCursor, ENT_QUOTES, 'UTF-8'), [ 'http', 'https' ] ); ?>">
							<?php echo $pageNumber; ?>
						</a>
					</li>
					<?php
					$data = unserialize (do_shortcode( '[next_page search="' . sanitize_text_field( $_GET['s'] ) . '" first="' . htmlspecialchars($per_page, ENT_QUOTES, 'UTF-8') . '" after="' . htmlspecialchars($endCursor, ENT_QUOTES, 'UTF-8') . '"]' ));
					
					$pagination = ! empty( $data['data']['products']['pageInfo'] ) ? $data['data']['products']['pageInfo'] : [];
					
					$hasNextPage = ! empty( $pagination['hasNextPage'] ) ? $pagination['hasNextPage'] : false;
					$endCursor = ! empty( $pagination['endCursor'] ) ? $pagination['endCursor'] : '';
					
				endwhile;
				if($currentPage['hasNextPage']):
				?>
					<li class="page-item">
						<a class="page-link" href="<?php echo sanitize_url( $trim . '&page=' . $currentPageNumber + 1 . '&after=' . $currentPage['endCursor'], [ 'http', 'https'] ); ?>">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<rect x="0.5" y="0.5" width="23" height="23" fill="#FBFBFB"/>
								<path d="M9.70492 6L8.29492 7.41L12.8749 12L8.29492 16.59L9.70492 18L15.7049 12L9.70492 6Z" fill="#262626"/>
								<rect x="0.5" y="0.5" width="23" height="23" stroke="#E0E0E0"/>
							</svg>
						</a>
				<?php
				else:
				?>
					<li class="page-item disabled">
				<?php
				endif;
				?>
					</li>
				</ul>
			</div>
		</div>		
		<?php
		endif;
		?>			
		
	</div>
</div>

<?php
get_footer();
?>
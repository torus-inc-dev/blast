jQuery('.single-item').slick({
    lazyLoad: 'onDemand',
    draggable: false,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                draggable: true
            }
        },
        {
            breakpoint: 480,
            settings: {
                draggable: true
            }
        }
    ]
});
jQuery('.categories-slick').slick({
    arrows: false,
    slidesToShow: 4,
    infinite: false,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                dots: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                dots: false
            }
        }
    ]
});
jQuery(document).ready(function($) {
    var searchResults = 
    '<li class="results">' +
        '<div class="card">' +
            '<a href="" class="fw-medium fs-3 btn btn-ghost-warning lh-1 text-uppercase my-3 px-0 pb-1 border-0 border-bottom-wide border-warning align-self-center" id="res">' + 
            'Pogledajte sve proizvode' + 
            '</a>' + 
        '</div>' +
    '</li>';
    $('#search-input').autocomplete({
        source: function(request, response) {
            $.ajax({
                dataType: 'json',
                url: AutocompleteSearch.ajax_url,
                data: {
                    term: request.term,
                    action: 'autocompleteSearch',
                    security: AutocompleteSearch.ajax_nonce,
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        select: function(event, ui) {
            window.location.href = ui.item.link;
        },
        open: function(event, ui) {
            $(this).autocomplete("widget").css("display", "grid");
            $(".ui-autocomplete").append(searchResults);
            $("#res").attr("href", "http://povratnik.torus.inc/?s=" + $("#search-input").val());
            
        },
        search: function(event, ui) {
          if($('.ui-autocomplete.ui-widget:visible').length) {
            $("#res").attr("href", "http://povratnik.torus.inc/?s=" + $("#search-input").val());
          }  
        },
        minLength: 3
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( 
            '<div class="card p-3 h-100" style="border: none; box-shadow: 0px 1px 4px 0px #1E293B0D;">' +
                '<span class="card-title text-muted m-0 p-0 fs-4 text-uppercase">' + item.category +
                '</span>' + 
                '<div class="lh-sm h-100 d-flex align-items-end justify-content-between">' +
                    '<span>' + item.label + '</span>' +
                    '<svg class="flex-shrink-0" style="float: right" width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 3L10.7663 4.23375L15.6488 9.125H5V10.875H15.6488L10.7663 15.7663L12 17L19 10L12 3Z" fill="#315FAD"/></svg>' + 
                '</div>' + 
            '</div>' )
          .appendTo( ul );
      };
    var searchResultsPhone = 
    '<li class="results">' +
        '<div class="card" style="border: none; box-shadow: 0px 1px 4px 0px #1E293B0D;">' +
            '<a href="" class="fw-medium fs-3 btn btn-ghost-warning lh-1 text-uppercase my-3 px-0 pb-1 border-0 border-bottom-wide border-warning align-self-center" id="res-phone">' + 
            'Pogledajte sve proizvode' + 
            '</a>' + 
        '</div>' +
    '</li>';
    
    $('#search-input-phone').autocomplete({
        source: function(request, response) {
            $.ajax({
                dataType: 'json',
                url: AutocompleteSearch.ajax_url,
                data: {
                    term: request.term,
                    action: 'autocompleteSearch',
                    security: AutocompleteSearch.ajax_nonce,
                },
                success: function(data) {
                    response(data.slice(0, 6));
                }
            });
        },
        select: function(event, ui) {
            window.location.href = ui.item.link;
        },
        open: function(event, ui) {
            $(".ui-autocomplete").append(searchResultsPhone);
            $("#res-phone").attr("href", "http://povratnik.torus.inc/?s=" + $("#search-input-phone").val());
            
        },
        search: function(event, ui) {
          if($('.ui-autocomplete.ui-widget:visible').length) {
            $("#res-phone").attr("href", "http://povratnik.torus.inc/?s=" + $("#search-input-phone").val());
          }  
        },
        minLength: 3
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( 
            '<div class="card p-3 h-100" style="border: none; box-shadow: 0px 1px 4px 0px #1E293B0D;">' +
                '<span class="card-title text-muted m-0 p-0 fs-4 text-uppercase">' + item.category +
                '</span>' + 
                '<div class="lh-sm h-100 d-flex align-items-end justify-content-between">' +
                    '<span>' + item.label + '</span>' +
                    '<svg class="flex-shrink-0" style="float: right" width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 3L10.7663 4.23375L15.6488 9.125H5V10.875H15.6488L10.7663 15.7663L12 17L19 10L12 3Z" fill="#315FAD"/></svg>' + 
                '</div>' + 
            '</div>' )
          .appendTo( ul );
      };
});
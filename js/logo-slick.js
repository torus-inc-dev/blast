jQuery('.media-slick').slick({
    arrows: false,
    slidesToShow: 6,
    slidesToScroll: 1,
    infinite: true,
    variableWidth: true,
    autoplay: true,
    autoplaySpeed: 3000,
    lazyLoad: 'onDemand',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 6,
                dots: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                dots: false
            }
        }
    ]
});
const buttonUpArray = document.querySelectorAll('#up');
buttonUpArray.forEach(element => element.addEventListener("click", increaseCartItemQuantity));
const buttonDownArray = document.querySelectorAll('#down');
buttonDownArray.forEach(element => element.addEventListener("click", decreaseCartItemQuantity));

function increaseCartItemQuantity() {
    this.parentNode.nextElementSibling.stepUp();
}

function decreaseCartItemQuantity() {
    if(this.parentNode.nextElementSibling.value > 1) {
        this.parentNode.nextElementSibling.stepDown();
    }
}
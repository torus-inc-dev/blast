jQuery('.products-slick').slick({
    arrows: false,
    slidesToShow: 5,
    infinite: false,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                dots: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                dots: false
            }
        }
    ]
});
<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blast
 */

get_header();
?>
	<div class="bg-fade">
		<div class="container container-xl">
			<div class="row">
				<span class="d-none d-sm-block position-absolute w-75 swiper-page fs-1 fw-medium">01</span>
				<div class="single-item">
					<div class="d-flex flex-column flex-lg-row align-items-center mx-0 py-0 justify-content-between">
						<div class="col-lg-5 offset-lg-1 col-12">
							<div>
								<h1 class="fw-bolder lh-sm mt-3 pt-lg-5">
									Stropne gredice raznih mjera
								</h1>
								<p class="fw-normal lead">
									Naručite stropne gredice po mjeri i osigurajte vrhunski građevinski materijal koji savršeno odgovara vašim potrebama
								</p>
								<a class="btn btn-ghost-warning fw-medium lh-1 fs-4 text-uppercase me-0 p-0 pb-1 border-0 border-bottom-wide border-warning">
									Više informacija
								</a>
							</div>
						</div>
						<div class="col-lg-5 offset-lg-1 col-12 px-3 px-md-0 pe-md-4 swiper-image">
							<img class="" src="./wp-content/themes/blast/media/swiper-img.png" alt="Gredice" loading="lazy">
						</div>
					</div>
					<div class="d-flex flex-column flex-lg-row align-items-center mx-0 py-0">
						<div class="col-lg-5 offset-lg-1 col-12">
							<div>
								<h1 class="fw-bolder lh-sm mt-3 pt-lg-5">
									Stropne gredice raznih mjera
								</h1>
								<span class="fw-normal lead">
									Naručite stropne gredice po mjeri i osigurajte vrhunski građevinski materijal koji savršeno odgovara vašim potrebama
								</span>
								<a class="btn btn-ghost-warning fw-medium lh-1 fs-4 text-uppercase me-0 p-0 pb-1 border-0 border-bottom-wide border-warning">
									Više informacija
								</a>
							</div>
						</div>
						<div class="col-lg-5 offset-lg-1 col-12 px-3 px-md-0 swiper-image">
							<img class="" src="./wp-content/themes/blast/media/swiper-img.png" alt="Gredice" loading="lazy">
						</div>
					</div>
				</div>
				<span class="d-sm-none m-0 w-75 swiper-page fs-1 lh-1 fw-medium">01</span>
			</div>
		</div>
	</div>
	
	<div class="container-xl p-0">
		<div class="row section-title mt-5 mb-3">
			<div class="col-lg-6 col-8 p-0">
				<h3 class="fw-medium mb-0 lh-base">
					Izdvojeno iz ponude
				</h3>
			</div>
			<div class="col-lg-6 col-4 d-flex p-0 align-self-sm-center">
				<a class="fw-medium btn btn-ghost-warning lh-1 fs-4 text-uppercase ms-auto me-0 p-0 pb-1 border-0 border-bottom-wide border-warning align-self-end">
					Pogledajte sve
				</a>
			</div>
		</div>
		<div class="row row-cards justify-content-between products-slick mt-0">

		<?php
		$data = unserialize (do_shortcode( '[featured_products_graphql]' ));
		
		$edges = ! empty( $data['data']['products']['edges'] ) ? $data['data']['products']['edges'] : [];
		
		foreach( $edges as $edge ) :
			$node = ! empty( $edge['node'] ) ? $edge['node'] : '';
			if ( ! empty($node) && is_array( $node ) ):
		?>

			<div class="card pb-2 border-0">
				<a href="<?php echo sanitize_url( $node['link'], [ 'http', 'https' ]); ?>">
					<div class="img-responsive img-responsive-3x2 card-img-top" style="background-image: url(<?php echo sanitize_url( $node['image']['sourceUrl'], [ 'http', 'https' ]); ?>); background-size: contain;">
					</div>
				</a>
				<div class="card-body p-3 mx-0 mt-0">
					<a href="<?php echo sanitize_url( $node['productCategories']['edges'][0]['node']['link'], [ 'http', 'https' ] ); ?>">
						<span class="card-title text-muted fs-4 mb-2 pb-1 text-uppercase"><?php echo sanitize_text_field( $node['productCategories']['edges'][0]['node']['name'] ); ?></span>
					</a>
					<h4 class="fw-medium lh-sm h-5 mb-0"><?php echo sanitize_text_field( $node['name'] ); ?></h4>
					<div class="d-flex justify-content-between ms-0 pb-2">
						<a class="btn btn-outline-info fw-medium fs-4 btn text-uppercase px-2" href="<?php echo sanitize_url( $node['link'], [ 'http', 'https' ] ); ?>">
							<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM11 15H9V9H11V15ZM11 7H9V5H11V7Z" fill="#315FAD"/>
							</svg>
							<span class="ms-2">Provjerite</span>
						</a>
						<a class="btn btn-primary px-2 py-1 lh-base">
							<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M19 13.8482H13V19.8482H11V13.8482H5V11.8482H11V5.84821H13V11.8482H19V13.8482Z" fill="#FFDC10"/>
							</svg>
						</a>
					</div>
				</div>
			</div>

		<?php
			endif;
		endforeach;
		?>

		</div>
	</div>
	<div class="container-xl">
		<div class="intersection p-0 mt-5"></div>
	</div>
	<div class="container-xl p-0">
		<div class="row section-title mt-5 mb-3">
			<div class="col-lg-6 col-8 p-0">
				<h3 class="fw-medium mb-0 lh-base">
					Kategorije proizvoda
				</h3>
			</div>
			<div class="col-lg-6 col-4 d-flex p-0">
				<a class="fw-medium btn btn-ghost-warning lh-1 fs-4 text-uppercase ms-auto me-0 p-0 pb-1 border-0 border-bottom-wide border-warning align-self-end">
					Pogledajte sve
				</a>
			</div>
		</div>
		<div class="row row-cards categories-slick mt-0">

		<?php
		$data = unserialize (do_shortcode( '[product_categories_query orderby="COUNT" first="4"]' ));
		
		$edges = ! empty( $data['data']['productCategories']['edges'] ) ? $data['data']['productCategories']['edges'] : [];
		
		foreach( $edges as $edge ) :
			$node = ! empty( $edge['node'] ) ? $edge['node'] : '';
			if ( ! empty($node) && is_array( $node ) ):
		?>

			<div class="card pb-2 border-0">
				<a href="<?php echo sanitize_url( $node['link'], [ 'http', 'https' ]); ?>">
					<div class="img-responsive img-responsive-6x4 card-img-top" style="background-image: url(<?php echo sanitize_url( $node['image']['sourceUrl'], [ 'http','https' ] ); ?>); background-size: contain;">
					</div>
				</a>
				<div class="card-body p-2 mx-1 mt-2">
					<div class="d-flex justify-content-between align-items-center ms-0 pb-0">
						<h4 class="fw-medium lh-sm mb-0"><?php echo sanitize_text_field( $node['name'] ); ?></h4>
						<a class="btn btn-outline-info px-2 py-1 lh-base" href="<?php echo sanitize_url( $node['link'], [ 'http', 'https' ]); ?>">
							<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M12 4.37134L10.59 5.78134L16.17 11.3713H4V13.3713H16.17L10.59 18.9613L12 20.3713L20 12.3713L12 4.37134Z" fill="#315FAD"/>
							</svg>
						</a>
					</div>
				</div>
			</div>

		<?php
			endif;
		endforeach;
		?>

		</div>
	</div>
	<div class="container-xl mt-5 px-0">
		<div class="row media-slick justify-content-between">
			
		<?php
		echo do_shortcode( '[company_logo_query search="logo"]' );
		?>

		</div>
	</div>
	<div class="container-xl p-0 scroll-top">
		<div class="row my-4">
			<div class="d-flex justify-content-end p-0">
				<a class="btn btn-outline-info p-2 lh-base" href="#">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M4 12L5.41 13.41L11 7.83L11 20L13 20L13 7.83L18.59 13.41L20 12L12 4L4 12Z" fill="#315FAD"/>
					</svg>
				</a>
			</div>
		</div>
	</div>
<?php
get_footer();

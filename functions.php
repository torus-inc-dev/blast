<?php
/**
 * Blast functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Blast
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function blast_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on Blast, use a find and replace
		* to change 'blast' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'blast', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'blast' ),
			'menu-2' => esc_html__( 'Secondary', 'blast' ),
			'menu-3' => esc_html__( 'Shop', 'blast' ),
			'menu-4' => esc_html__( 'Categories', 'blast' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'blast_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'blast_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function blast_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'blast_content_width', 640 );
}
add_action( 'after_setup_theme', 'blast_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function blast_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'blast' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'blast' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'blast_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function blast_scripts() {
	if( is_home() || is_product() || is_page('o-nama')):
		wp_register_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
		wp_enqueue_style('slick');
		wp_register_style( 'slick', get_stylesheet_directory_uri() . '/css/slick-theme.css', array(), _S_VERSION, true );
		wp_enqueue_style( 'slick-theme');
		wp_register_script('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery', 'jquery-migrate'], null, true);
		wp_enqueue_script('slick');
	endif;

	if( is_home() ):	
		wp_register_script('slick-scripts', get_template_directory_uri() . '/js/slick.js', ['slick' ], null, true);
		wp_enqueue_script('slick-scripts');
	endif;

	if( is_home() || is_product() ):
		wp_register_script('products-slick', get_template_directory_uri() . '/js/products-slick.js', ['slick' ], null, true);
		wp_enqueue_script('products-slick');
	endif;

	if( is_home() || is_page('o-nama')):
		wp_register_script('logo-slick', get_template_directory_uri() . '/js/logo-slick.js', ['slick' ], null, true);
		wp_enqueue_script('logo-slick');
	endif;

	wp_register_script('tabler', 'https://cdn.jsdelivr.net/npm/@tabler/core@1.0.0-beta17/dist/js/tabler.min.js');
	wp_enqueue_script('tabler');
	wp_register_style( 'tabler', 'https://cdn.jsdelivr.net/npm/@tabler/core@1.0.0-beta17/dist/css/tabler.min.css' );
	wp_enqueue_style('tabler');
	wp_register_script('quantity', get_template_directory_uri() . '/js/quantity-button.js', array(), null, true);
	wp_enqueue_script('quantity');

	wp_enqueue_style( 'blast-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'blast-style', 'rtl', 'replace' );

	wp_enqueue_script( 'blast-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	// wp_enqueue_script('jquery-ui-autocomplete');

	wp_enqueue_script('autocomplete-search', get_template_directory_uri() . '/js/autocomplete.js', ['jquery', 'jquery-ui-autocomplete'], null, true);
	
	wp_localize_script('autocomplete-search', 'AutocompleteSearch', [
		'ajax_url' => admin_url('admin-ajax.php'),
		'ajax_nonce' => wp_create_nonce('autocompleteSearchNonce')
	]);

	wp_enqueue_style('jquery-ui', '//code.jquery.com/ui/' . wp_scripts()->registered['jquery-ui-autocomplete']->ver . '/themes/smoothness/jquery-ui.css', false, null, false);


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'blast_scripts' );

function company_schema() {
	$schema = 
	[
		'@context' => 'https://schema.org',
		'@type' => 'HomeAndConstructionBusiness',
		'address' => [
		  '@type' => 'PostalAddress',
		  'addressCountry' => 'Hrvatska',
		  'addressLocality' => 'Vukovar',
		  'postalCode' => '32000',
		  'streetAddress' => 'Priljevo 203d',
		],
		'description' => 'Proizvodnja gredica i prodaja drva za ogrjev, te prodaja građevinskog materijala',
		'name' => 'T.O. Povratnik',
		'email' => 'mailto:webshop@povratnik.hr',
		'openingHours' => [
		  0 => 'Mo-Fr 07:00-15:00',
		  1 => 'Sa 07:00-13:00',
		],
		'telephone' => '+38532432342',
		'telephone' => '+38598518819',
		'currenciesAccepted' => 'EUR',
	];
	$output = json_encode( $schema, JSON_UNESCAPED_SLASHES );
	?>
	<script type="application/ld+json">
		<?= $output; ?>
	</script>
	<?php
}
add_action( 'wp_footer', 'company_schema' );

add_action('wp_ajax_nopriv_autocompleteSearch', 'awp_autocomplete_search');
add_action('wp_ajax_autocompleteSearch', 'awp_autocomplete_search');
function awp_autocomplete_search() {
	check_ajax_referer('autocompleteSearchNonce', 'security');
 
	$search_term = $_REQUEST['term'];
	if (!isset($_REQUEST['term'])) {
		//echo json_encode([]);
	}
 
	$suggestions = [];

	$query = '
	query SearchForm($search: String = "", $first: Int) {
		products(where: {search: $search, orderby: {field: NAME, order: ASC}}, first: $first) {
		  	edges {
				node {
			  		... on VariableProduct {
          				databaseId
			  			link
			  			name
						productCategories {
							edges {
								node {
									name
								}
							}
						}
        			}
        			... on SimpleProduct {
          				databaseId
			  			link
			  			name
						productCategories {
							edges {
								node {
									name
								}
							}
						}
        			}
				}
		  	}
		}
	}';

	$variables = [
		'search' => $search_term,
		'first' => 9
	];
	
	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	if( ! empty( $data['data']['products']['edges'] ) ):
		foreach($data['data']['products']['edges'] as $edge):
			$node = ! empty( $edge['node'] ) ? $edge['node'] : '';
			$suggestions[] = [
				'id' => $node['databaseId'],
				'label' => $node['name'],
				'link' => $node['link'],
				'category' => $node['productCategories']['edges'][0]['node']['name']
			];
		endforeach;
	endif;

	// if ($query->have_posts()) {
	// 	while ($query->have_posts()) {
	// 		$query->the_post();
	// 		$suggestions[] = [
	// 			'id' => get_the_ID(),
	// 			'label' => get_the_title(),
	// 			'link' => get_the_permalink()
	// 		];
	// 	}
	// 	wp_reset_postdata();
	// }

	echo json_encode($suggestions);
	wp_die();
	
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

add_filter( 'nav_menu_css_class', '__return_empty_array' );

function smartwp_remove_wp_block_library_css(){
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-blocks-style' ); // Remove WooCommerce block CSS
   } 
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options –> Reading
  // Return the number of products you wanna show per page.
  $cols = 3;
  return $cols;
}

add_filter('header_query', 'query_menu_items');
function query_menu_items( $id ) {
	$query = '
	query MenuItems($id: ID = "", $idType: MenuNodeIdTypeEnum = NAME) {
		menu(id: $id, idType: $idType) {
			name
			menuItems {
				nodes {
					label
					url
				}
			}
		}
	}';

	$variables = [
		'id' => ! empty( $id ) ? $id : ''
	];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	$data = serialize($data);
	return ! empty( $data ) ? $data : '';
}

add_shortcode( 'query_page', function( $atts ) {
	$query = '
		query Page($uri: String = "") {
			nodeByUri(uri: $uri) {
				__typename
				... on ContentType {
					uri
				}
			}
		}';

	$variables = [
		'uri' => ! empty( $atts['uri'] ) ? $atts['uri'] : ''
	];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	$data = $data['data']['nodeByUri']['uri'];

	return ! empty( $data ) ? $data : '';
} );

add_shortcode( 'company_logo_query' , function( $atts )  {
	
	$query = 'query MediaItems ($search: String = "") {
		mediaItems(where: {search: $search}) {
			edges {
				node {
					title
					sourceUrl
				}
			}
		}
	}';

	$variables = [
		'search' => ! empty( $atts['search'] ) ? $atts['search'] : ''
	];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	ob_start();

	$edges = ! empty( $data['data']['mediaItems']['edges'] ) ? $data['data']['mediaItems']['edges'] : [];
	foreach( $edges as $edge ) :
		$node = ! empty( $edge['node'] ) ? $edge['node'] : '';
		?>
		<img class="align-self-center" src="<?php echo sanitize_url( $node['sourceUrl'], [ 'http', 'https' ] ); ?>"/>
	<?php
	endforeach;

	return ob_get_clean();
});

add_shortcode( 'product_category_query', function( $atts ) {
	
	$query = '
	query Categories($id: ID = "", $idType: ProductCategoryIdType = SLUG) {
		productCategory(id: $id, idType: $idType) {
			name
		}
	}';
	$variables = [
		'id' => ! empty( $atts['id'] ) ? $atts['id'] : ''
	];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);
 
	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});

add_shortcode( 'product_categories_query', function( $atts ) {

	$query = 'query Categories($orderby: TermObjectsConnectionOrderbyEnum , $first: Int) {
		productCategories(where: {orderby: $orderby}, first: $first) {
			edges {
				node {
					name
					image {
						sourceUrl
					}
					uri
					link
					slug
				}
			}
		}
	}';

	$variables = [
		'orderby' => ! empty( $atts['orderby'] ) ? $atts['orderby'] : null,
		'first' => ! empty( $atts['first'] ) ? absint( $atts['first'] ) : null
	];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);
 
	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});


add_shortcode( 'featured_products_graphql', function( $atts ) {
	
	$query = '
		query FeaturedProducts($featured: Boolean) {
			products(where: {featured: $featured}) {
				edges {
					node {
						name
						uri
						link
						productCategories {
							edges {
								node {
									id
									name
									uri
									link
								}
							}
						}
						image {
							sourceUrl
						}
					}
				}
			}
		}';
		
		
	$variables = [
		'featured' => true
	];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);
	
	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});

add_shortcode( 'product_search_query', function( $atts ) {
	
	$query = '
	query SearchProducts($search: String = "", $first: Int, $last: Int, $before: String="", $after: String="") {
		products(where: {search: $search, orderby: {field: NAME, order: ASC}}, first: $first, last: $last, before:$before, after: $after) {
			edges {
				node {
					... on VariableProduct {
						link
						name
						image {
							sourceUrl
						}
						productCategories {
							edges {
								node {
									link
									name
								}
							}
						}
					}
					... on SimpleProduct {
						link
						name
						image {
							sourceUrl
						}
						productCategories {
							edges {
								node {
									link
									name
								}
							}
						}
					}
				}
			}
			pageInfo {
				hasNextPage
				endCursor
				hasPreviousPage
				startCursor
			}
		}
	}';
	$variables = [
		'search' => ! empty( $atts['search'] ) ? $atts['search'] : '',
		'first' => null ? null : absint( $atts['first'] ),
		'last' => null ? null : absint( $atts['last'] ),
		'order' => null ? null : $atts['order'],
		'before' => null ? null : $atts['before'],
		'after' => null ? null : $atts['after']
	];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);
 
	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});

add_shortcode( 'next_page', function( $atts ) {
	
	$query = '
		query nextPage($search: String = "", $id: ID!, $idType: ProductCategoryIdType = SLUG, $first: Int, $after: String = "") {
			productCategory( id: $id, idType: $idType ) {
				products(first: $first, where: {orderby: {field: NAME, order: ASC}}, after: $after) {
					pageInfo {
						hasNextPage
						endCursor
					}
				}
			}
			products(first: $first, where: {search: $search, orderby: {field: NAME, order: ASC}}, after: $after) {
				pageInfo {
					hasNextPage
					endCursor
				}
			}
		}';
	$variables = [
		'search' => ! empty( $atts['search'] ) ? $atts['search'] : '',
		'id' => ! empty( $atts['id'] ) ? $atts['id'] : '',
		'first' => ! empty( $atts['first'] ) ? absint( $atts['first'] ) : 9,
		'after' => ! empty( $atts['after'] ) ? $atts['after'] : '',
	];
	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);
 
	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});

add_shortcode( 'previous_page', function( $atts ) {
	
	$query = '
	query previousPage($search: String = "", $id: ID!, $idType: ProductCategoryIdType = SLUG, $last: Int, $before: String = "") {
		productCategory(id: $id, idType: $idType) {
			products(last: $last, where: {orderby: {field: NAME, order: DESC}}, before:$before) {
			nodes{name}
				pageInfo {
					hasPreviousPage
					startCursor
				}
			}
		}
		products(last: $last, where: {search: $search, orderby: {field: NAME, order: DESC}}, before: $before) {
			pageInfo {
				hasPreviousPage
				startCursor
			}
		}
	}';
	$variables = [
		'search' => ! empty( $atts['search'] ) ? $atts['search'] : '',
		'id' => ! empty( $atts['id'] ) ? $atts['id'] : '',
		'last' => ! empty( $atts['last'] ) ? absint( $atts['last'] ) : 9,
		'before' => ! empty( $atts['before'] ) ? $atts['before'] : ''
	];
	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});

add_shortcode( 'product_category_fetch', function( $atts ) {
	
$query = '
query categoryProducts($id: ID!, $idType: ProductCategoryIdType = SLUG, $first: Int, $last: Int, $before: String = "", $after: String = "", $order: OrderEnum) {
	productCategory(id: $id, idType: $idType) {
		name
		uri
		link
		slug
		count
		products(first: $first,last: $last, where: {orderby: {field: NAME, order: $order}}, before:$before, after: $after) {
			pageInfo {
				hasNextPage
				hasPreviousPage
				startCursor
				endCursor
			}
			edges {
				node {
					link
					name
					uri
					image {
						sourceUrl
					}
					... on VariableProduct {
						id
						name
					}
				}
			}			
		}
	}
}';

$variables = [
	'id' => $atts['id'],
	'first' => null ? null : absint( $atts['first'] ),
	'last' => null ? null : absint( $atts['last'] ),
	'order' => null ? null : $atts['order'],
	'before' => null ? null : $atts['before'],
	'after' => null ? null : $atts['after']
];

$data = graphql( 
	[
		'query' => $query,
		'variables' => $variables,
		'operation_name' => ''
	] 
);

$data = serialize($data);

return ! empty( $data ) ? $data : '';
});

add_shortcode( 'product_fetch', function( $atts ) {
	
	$query = '
	query product($id: ID = "", $idType: ProductIdTypeEnum = SLUG) {
		product(id: $id, idType: $idType) {
			allPaUnit {
				nodes {
				  name
				  slug
				}
			}
			productCategories {
				edges {
			  		node {
						name
						link
			  		}
				}
		  	}
		  	image {
				sourceUrl
		  	}
		  	name
			link
			databaseId
		  	description
		  	onSale
		  	dateOnSaleTo
    		localAttributes {
      			edges {
					node {
						options
						name
						label
					}
				}
    		}
			upsell {
    			edges {
        			node {
          				... on VariableProduct {
            				name
							link
							image {
					  			sourceUrl
							}
							productCategories {
								edges {
									node {
										link
										name
									}
								}
							}
          				}
          				... on SimpleProduct {
            				name
							link
							image {
					  			sourceUrl
							}
							productCategories {
								edges {
									node {
										link
										name
									}
								}
							}
        				}
        			}
      			}
    		}
		}
	}';
	$variables = [
		'id' => ! empty( $atts['id'] ) ? $atts['id'] : ''
	];
	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});

add_filter( 'cart_items', 'query_cart_items');
function query_cart_items() {

	$fragment = '
	fragment ProductContentSlice on Product {
		id
		name
		productId
		globalAttributes {
			edges {
				node {
					options
					name
				}
			}
		}
		attributes {
			nodes {
				variation
				name
			}
		}
	}
	
	fragment CartItemContent on CartItem{
		key
		product {
			node {
				...ProductContentSlice
			}
			simpleVariations {
				value
			}
		}
		quantity
		extraData {
			key
			value
		}
		variation {
			attributes {
			  	name
			}
		}
	}
	
	fragment CartContent on Cart {
		contents {
			itemCount
			nodes {
				...CartItemContent
			}
		}
	}';
	
	$query = '
	query GetCart {
		cart {
			...CartContent
		}
	}' . $fragment;

	$variables = [];

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
};

add_shortcode( 'remove_items_from_cart', function( $atts ) {

	$fragment = '
	fragment ProductContentSlice on Product {
		id
		name
		globalAttributes {
			edges {
				node {
					options
					name
				}
			}
		}
	}
	
	fragment CartItemContent on CartItem{
		key
		product {
			node {
				...ProductContentSlice
			}
		}
		quantity
		extraData {
			key
			value
		}
	}
	
	fragment CartContent on Cart {
		contents {
			itemCount
			nodes {
				...CartItemContent
			}
		}
	}';
	
	$query = '
	mutation RemoveItemsFromCart($keys: [ID], $all: Boolean, $clientMutationId: String = "") {
		removeItemsFromCart(input: {keys: $keys, all: $all, clientMutationId: $clientMutationId}) {
		  cart {
			...CartContent
		  }
		  cartItems {
			...CartItemContent
		  }
		}
	}' . $fragment;

	$keys = [$atts['keys']];
	
	$variables = [
		'keys' => ! empty( $keys ) ? $keys : '',
		'all' => ($atts['all'] === 'true') ? true : false,
		'clientMutationId' => ! empty( $atts['clientMutationId'] ) ? $atts['clientMutationId'] : ''
	];
	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);

	$data = serialize($data);

	return ! empty( $data ) ? $data : '';
});

add_shortcode( 'homepage' , function($atts) {
	$query = '{
		nodeByUri(uri: "/") {
			__typename
			... on ContentType {
				id
				name
			}
		  	... on Page {
				id
				title
		  	}
		}
	}';
});

add_shortcode( 'shop_page_2' , function($atts) {
	$query = '{
		nodeByUri(uri: "shop") {
			__typename
			... on ContentType {
				id
				name
			}
		  	... on Page {
				id
				title
		  	}
		}
	}';

	$data = graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);
});

add_filter ('woocommerce_add_to_cart_form_action', 'add_product_to_cart', 1);
function add_product_to_cart( $redirect ) {
    if ( isset( $_POST['addToCart'] ) ):

		$attributes = [];
		
		if(isset( $_POST['attribute-name'] )):
			for($i = 0; $i < sizeof($_POST['attribute-name']); $i++):
				$attributes[] = 
					array( 
						'attributeName' => sanitize_text_field($_POST['attribute-name'][$i]),
						'attributeValue' => sanitize_text_field($_POST['attribute-value'][$i])
					);
			endfor;
		endif;

		$query = '
		mutation AddToCart($productId: Int!, $quantity: Int, $extraData: String, $clientMutationId: String = "", $variation: [ProductAttributeInput]) {
			addToCart(input: {productId: $productId, quantity: $quantity, extraData: $extraData, clientMutationId: $clientMutationId, variation: $variation}) {
				cartItem {
					product {
						node {
							name
						}
					}
				}
				clientMutationId
			}
		}';
		
		$variables = [
			'productId' => ! empty( $_POST['product-id'] ) ? (int)sanitize_text_field($_POST['product-id']) : '',
			'quantity' => ! empty( $_POST['quantity'] ) ? (int)sanitize_text_field($_POST['quantity']) : '',
			'clientMutationId' => uniqid('',true),
			'variation' => ! empty( $attributes ) ? $attributes : []
		];

		graphql( 
			[
				'query' => $query,
				'variables' => $variables,
				'operation_name' => ''
			] 
		);

        //$redirect = sanitize_url((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", [ 'http', 'https' ] );
        
		//wp_redirect($redirect);
		unset( $_POST['addToCart'] );
		return $redirect;
        exit;
	endif;
}

add_action ('wp_loaded', 'remove_item_from_cart');
function remove_item_from_cart() {
	if (isset($_POST['remove-from-cart'])):
		do_shortcode( '[remove_items_from_cart keys="' . sanitize_text_field($_POST['remove-from-cart'][0]) . '" all=false clientMutationId="' . uniqid('',true) . '"]');
		$redirect = sanitize_url((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", [ 'http', 'https' ] );
		wp_redirect($redirect);
		exit;
	endif;
}

add_action ('send_headers', 'create_order');
function create_order() {
	if (isset($_POST['create-order'])):
		//var_dump(wc_get_order($order_id)->get_customer_id());

		// $items = [];
		// for($i = 0; $i < sizeof($_POST['product-id']); $i++):
		// 	$items[] = 
		// 		array( 
		// 			'productId' => (int)sanitize_text_field($_POST['product-id'][$i]),
		// 			'quantity' => (int)sanitize_text_field($_POST['quantity'][$i])
		// 		);
		// endfor;

		$query = '
		mutation Checkout($clientMutationId: String = "", $customerNote: String = "", $email: String = "") {
			checkout(input: {clientMutationId: $clientMutationId, customerNote: $customerNote, billing: {email: $email}}) {
			  	clientMutationId
				order {
					id
				}
				redirect
			}
		}';
		

		$variables = [
			'clientMutationId' => uniqid('',true),
			'customerNote' => sanitize_text_field($_POST['customer-note']),
			'email' => sanitize_text_field($_POST['email'])
		];
		
		$data = graphql( 
			[
				'query' => $query,
				'variables' => $variables,
				'operation_name' => ''
			] 
		);
		

		// $query = '
		// mutation EmptyCart($clientMutationId: String = "") {
		// 	emptyCart(input: {clientMutationId: $clientMutationId}) {
		// 	  	cart {
		// 			isEmpty
		// 	  	}
		// 	}
		// }';

		// $variables = [
		// 	'clientMutationId' => uniqid('',true)
		// ];

		// graphql( 
		// 	[
		// 		'query' => $query,
		// 		'variables' => $variables,
		// 		'operation_name' => ''
		// 	] 
		// );

		/*$query = '
		mutation CreateOrder($status: OrderStatusEnum, $clientMutationId: String = "", $isPaid: Boolean, $lineItems: [LineItemInput], $customerNote: String = "", $email: String = "") {
			createOrder(input: {status: $status, clientMutationId: $clientMutationId, isPaid: $isPaid, lineItems: $lineItems, customerNote: $customerNote, billing: {email: $email}}) {
				order {
					status
				}
				clientMutationId
				orderId
			}
		}';
		

		$variables = [
			'status' => 'PENDING',
			'clientMutationId' => uniqid('',true),
			'isPaid' => false,
			'lineItems' => ! empty( $items ) ? $items : '',
			'customerNote' => ! empty( $_POST['customer-note'] ) ? htmlspecialchars($_POST['customer-note']) : '',
			'email' => htmlspecialchars($_POST['email'])
		];
		
		graphql( 
			[
				'query' => $query,
				'variables' => $variables,
				'operation_name' => ''
			] 
		);*/

		//$redirect = sanitize_url((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]", [ 'http', 'https' ] );
		wp_redirect(sanitize_url($data['data']['checkout']['redirect'], [ 'http', 'https' ]));
		exit;
	endif;
}

function http_headers() {
	if(is_user_logged_in() || is_admin()) {
		return;
	}
	
	
}

add_action('woocommerce_init', 'http_headers',20);


add_filter('woocommerce_cart_needs_payment','__return_false');

add_action('woocommerce_thankyou', function ($order_id) {



		$query = 'mutation MyMutation($email: String = "") {
  updateCustomer(input: {billing: {email: $email}}) {
    authToken
    clientMutationId
    refreshToken
  }
}';
		$variables = [
			'email' => wc_get_order( $order_id )->get_billing_email()
		];

		var_dump(graphql( 
			[
				'query' => $query,
				'variables' => $variables,
				'operation_name' => ''
			] 
		));
	
	$query = '
	mutation UpdateOrderStatus($orderId: Int) {
		updateOrder(input: {status: CHECKOUT_DRAFT, orderId: $orderId, clientMutationId: ""}) {
			order {
				id
			}
		}
	}';

	$variables = [
		'clientMutationId' => uniqid('',true),
		'orderId' => $order_id
	];

	graphql( 
		[
			'query' => $query,
			'variables' => $variables,
			'operation_name' => ''
		] 
	);
});

add_filter( 'use_widgets_block_editor', '__return_false' );

add_filter( 'woocommerce_is_purchasable', '__return_true' );

add_filter('body_class','drop_page_number_class');
function drop_page_number_class($classes) {
    global $wp_query;

    $arr = array();

    if(is_page()) {
    $page_id = $wp_query->get_queried_object_id();
    $arr[] = 'page-template-default page-' . $page_id . ' theme-blast woocommerce-js woocommerce-active';
    }

    return $arr;
}

add_action('wp_loaded', 'custom_form_submit');

function custom_form_submit() {
	if(isset($_POST['send'])):
	$query = '
	mutation SendEmail($subject: String = "", $body: String = "", $replyTo: String = "", $clientMutationId: String = "") {
		sendEmail(
		  input: {subject: $subject, body: $body, replyTo: $replyTo, clientMutationId: $clientMutationId}
		) {
		  origin
		  sent
		  message
		}
	  }';
		

		$variables = [
			'replyTo' => sanitize_text_field($_POST['replyTo']),
			'subject' => sanitize_text_field($_POST['subject']),
			'body' => sanitize_text_field($_POST['body']),
			'clientMutationId' => uniqid('',true)
		];
		
		graphql( 
			[
				'query' => $query,
				'variables' => $variables,
				'operation_name' => ''
			] 
		);
		
		wp_redirect( sanitize_url ((empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", [ 'http', 'https' ] ) );
	
		exit;
		
	endif;

}

add_action('wp_footer', 'hide_admin_bar_prefs');
function hide_admin_bar_prefs() {
    $op = '
    <style type="text/css">
        html {margin-top: 0px !important;}
        #wpadminbar {display: none;}
    </style> ';
    echo $op;
}

add_action('woocommerce_init', 'guest_session_cookies',1);
function guest_session_cookies() {
	if(is_user_logged_in() || is_admin()) {
		return;
	}
	if(isset(WC()->session)) {
		if(!WC()->session->has_session()) {
			WC()->session->set_customer_session_cookie(60);
		}
			$session_cookie = WC()->session->get_session_cookie();	

		$headers['woocommerce-session'] = 'Session ' . $session_cookie[3];
	// var_dump($headers);
	//header(apply_filters('graphql_send_header', sprintf('%s: %s', 'woocommerce-session', $session_cookie[3]), 'woocommerce-session', $session_cookie[3]));
	header(sprintf('%s: %s', 'Content-Type', 'application/json'));
	header(sprintf('%s: %s', 'woocommerce-session', $headers['woocommerce-session']));
	}
}